package net.seliba.eastern.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;

public class PlayerPortalEnterListener implements Listener {

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlayerPortalEnter(PlayerPortalEvent event) {
    event.setCancelled(true);
    event.setTo(event.getFrom());
  }

}
