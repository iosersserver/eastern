package net.seliba.eastern.listener;

import net.seliba.eastern.entities.EggLocation;
import net.seliba.eastern.utils.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    event.setJoinMessage("§8[§a+§8] §7" + event.getPlayer().getName());
    EggLocation.handleJoin(event.getPlayer());
    Scoreboard.updateAllScoreboards();
  }

}
