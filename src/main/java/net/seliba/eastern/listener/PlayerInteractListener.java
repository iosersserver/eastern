package net.seliba.eastern.listener;

import java.util.HashMap;
import java.util.Map;
import net.seliba.eastern.entities.EggLocation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

  /*
    onInteract: Check ob Location schon von Spieler geclaimt wurde
    Wenn true: Location claimen, zufällige Belohnung geben
    Regelmäßig: Partikel für nicht geclaimte Sachen
   */

  private final Map<Player, Long> lastInteraction = new HashMap<>();

  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent event) {
    if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
      Player player = event.getPlayer();
      if (lastInteraction.containsKey(player)) {
        if (lastInteraction.get(player) + 100 > System.currentTimeMillis()) {
          if (EggLocation.skullMaterials.contains(event.getClickedBlock().getType())) {
            return;
          }
          event.setCancelled(true);
          return;
        }
      }
      lastInteraction.put(player, System.currentTimeMillis());
      if (isBlocked(event.getClickedBlock().getType())) {
        event.setCancelled(true);
        if (EggLocation.skullMaterials.contains(event.getClickedBlock().getType())) {
          EggLocation.handleClick(event.getClickedBlock().getLocation(), player);
        }
      }
      return;
    }
    event.setCancelled(true);
  }

  private boolean isBlocked(Material material) {
    return !(material.name().contains("BUTTON") || material.name().contains("DOOR") || material.name().contains("PRESSURE") || material.name().contains("FENCE"));
  }

}
