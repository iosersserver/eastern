package net.seliba.eastern.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;

public class RedstoneListener implements Listener {

  @EventHandler
  public void onRedstone(BlockRedstoneEvent event) {
    if (isBlocked(event.getBlock().getType())) {
      event.setNewCurrent(event.getOldCurrent());
    }
  }

  private boolean isBlocked(Material material) {
    return !(material.name().contains("BUTTON") || material.name().contains("DOOR") || material.name().contains("PREASURE"));
  }

}
