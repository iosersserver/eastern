package net.seliba.eastern.listener;

import net.seliba.eastern.Eastern;
import net.seliba.eastern.utils.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event) {
    event.setQuitMessage("§8[§c-§8] §7" + event.getPlayer().getName());
    Bukkit.getScheduler().runTaskLater(Eastern.getPlugin(Eastern.class), Scoreboard::updateAllScoreboards, 40L);
  }

}
