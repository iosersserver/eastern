package net.seliba.eastern.utils;

import net.seliba.eastern.entities.EggLocation;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.RenderType;

public class Scoreboard {

  private static void updateScoreboard(Player player) {
    org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    //Objective sidebar = scoreboard.registerNewObjective(player.getUniqueId().toString().split("-")[0] + String.valueOf(System.currentTimeMillis()).substring(8), player.getUniqueId().toString().split("-")[0] + String.valueOf(System.currentTimeMillis()).substring(8));
    Objective sidebar = scoreboard.registerNewObjective("abc", "def", "§e§nOsterevent", RenderType.INTEGER);
    sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);

    //sidebar.setDisplayName("§e§nOsterevent");
    sidebar.getScore("§aSpieler online").setScore(11);
    sidebar.getScore("§7» §6" + Bukkit.getOnlinePlayers().size()).setScore(10);
    sidebar.getScore(" ").setScore(9);
    sidebar.getScore("§aDeine gefundenen Eier").setScore(8);
    sidebar.getScore("§7» §6" + EggLocation.getEggs(player) + " / " + EggLocation.easterEggClaimers.size()).setScore(7);
    sidebar.getScore("  ").setScore(6);
    sidebar.getScore("§aInsgesamt gefundene Eier").setScore(5);
    sidebar.getScore("§7» §6" + EggLocation.totalFoundEggs() + " / " + EggLocation.playerEggsFound.size() * EggLocation.easterEggClaimers.size() + " ").setScore(4);
    sidebar.getScore("   ").setScore(3);
    sidebar.getScore("§aDeine Platzierung").setScore(2);
    int placement = EggLocation.totalPlacement(player);
    sidebar.getScore("§7» §6" + (placement == -1 || placement == 0 ? "Keine Platzierung" : placement + ". Platz") + "   ").setScore(1);

    player.setScoreboard(scoreboard);
  }

  public static void updateAllScoreboards() {
    Bukkit.getOnlinePlayers().forEach(Scoreboard::updateScoreboard);
  }

}
