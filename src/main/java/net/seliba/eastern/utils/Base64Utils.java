package net.seliba.eastern.utils;

import org.apache.commons.codec.binary.Base64;

public class Base64Utils {

  public static String encode(String text) {
    return Base64.encodeBase64String(text.getBytes());
  }

  public static String decode(String text) {
    return new String(Base64.decodeBase64(text));
  }

}
