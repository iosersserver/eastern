package net.seliba.eastern.entities;

import net.seliba.eastern.Eastern;
import net.seliba.eastern.utils.Base64Utils;
import net.seliba.eastern.utils.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class EggLocation {

  public static final Map<Location, List<String>> easterEggClaimers = new HashMap<>();
  public static final Map<String, Integer> playerEggsFound = new HashMap<>();
  private static final ExecutorService executorService = Executors.newFixedThreadPool(10);
  public final static List<Material> skullMaterials = Arrays.asList(Material.PLAYER_HEAD, Material.PLAYER_WALL_HEAD, Material.SKELETON_SKULL);
  private final static double X_MIN = 1332.0;
  private final static double X_MAX = 1520.0;
  private final static double Y_MIN = 0.0;
  private final static double Y_MAX = 63.0;
  private final static double Z_MIN = 120.0;
  private final static double Z_MAX = 308.0;

  public static void initialize() {
    Bukkit.getScheduler().runTaskLater(Eastern.getPlugin(Eastern.class), () -> {
        if (Eastern.getDataFile().getKeys(true).size() != 0) {
          System.out.println("Loaded from file!");
          Eastern.getDataFile().getConfigurationSection("locations").getKeys(true).forEach(section -> {
            String decodedSection = Base64Utils.decode(section);
            easterEggClaimers.put(new Location(Bukkit.getWorld("world"), Double.parseDouble(decodedSection.split("-")[0]), Double.parseDouble(decodedSection.split("-")[1]), Double.parseDouble(decodedSection.split("-")[2])), Eastern.getDataFile().getStringList("locations." + section));
          });
          Eastern.getRewardsFile().getConfigurationSection("").getKeys(true).forEach(section ->
            playerEggsFound.put(section, 200 - Eastern.getRewardsFile().getStringList(section).size())
          );
          for (Location location : easterEggClaimers.keySet()) {
            if (!skullMaterials.contains(location.getBlock().getType())) {
              location.getBlock().setType(Material.SKELETON_SKULL);
            }
          }
        } else {
          Bukkit.getScheduler().runTask(Eastern.getPlugin(Eastern.class), () -> {
            AtomicInteger count = new AtomicInteger();
            for(double x = X_MIN; x <= X_MAX; x++) {
              double finalX = x;
              executorService.execute(() -> {
                for(double y = Y_MIN; y <= Y_MAX; y++) {
                  for(double z = Z_MIN; z <= Z_MAX; z++) {
                    Location location = new Location(Bukkit.getWorld("world"), finalX, y, z);
                    if (!skullMaterials.contains(location.getBlock().getType())) {
                      continue;
                    }
                    String path = "locations." + Base64Utils.encode(Math.abs(finalX) + "-" + Math.abs(y) + "-" + Math.abs(z));

                    if (!Eastern.getDataFile().contains(path)) {
                      Eastern.getDataFile().set(path, new ArrayList<>());
                      try {
                        Eastern.getDataFile().save();
                      } catch (Exception exception) {
                        System.out.println(finalX + "." + y + "." + z);
                      }
                    }
                    List<String> eggClaimers = Eastern.getDataFile().getStringList(path);
                    easterEggClaimers.put(location, eggClaimers);
                    count.getAndIncrement();
                    try {
                      Thread.sleep(10);
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                }
              });
            }
            System.out.println("Finished scanning! Found " + count.get());
          });
        }
    }, 0L);
  }

  public static void handleClick(Location location, Player player) {
    if (!easterEggClaimers.containsKey(location)) {
      return;
    }

    List<String> claimers = easterEggClaimers.get(location);
    if (claimers.contains(player.getUniqueId().toString())) {
      player.sendMessage("§cDu hast dieses Ei bereits eingesammelt!");
      player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_HIT, 10f, 10f);
      return;
    }

    claimers.add(player.getUniqueId().toString());
    easterEggClaimers.put(location, claimers);
    playerEggsFound.put(player.getUniqueId().toString(), playerEggsFound.getOrDefault(player.getUniqueId().toString(), 0) + 1);

    String path = "locations." + Base64Utils.encode(Math.abs(location.getX()) + "-" + Math.abs(location.getY()) + "-" + Math.abs(location.getZ()));
    Eastern.getDataFile().set(path, easterEggClaimers.get(location));
    Eastern.getDataFile().save();

    String reward = addReward(player);
    player.sendMessage("§aDu hast ein Ei gefunden! Inhalt: §6" + reward);
    Scoreboard.updateAllScoreboards();
  }

  public static void handleJoin(Player player) {
    if (Eastern.getRewardsFile().get(player.getUniqueId().toString()) != null) {
      return;
    }
    List<String> playerRewards = new ArrayList<>();
    for (Reward value : Reward.values()) {
      for (int i = 0; i < value.amount; i++) {
        playerRewards.add(value.name());
      }
    }
    Eastern.getRewardsFile().set(player.getUniqueId().toString(), playerRewards);
    Eastern.getRewardsFile().save();
  }

  private static String addReward(Player player) {
    List<String> possibleRewards = Eastern.getRewardsFile().getStringList(player.getUniqueId().toString());
    if (possibleRewards.isEmpty()) {
      player.sendMessage("§cEin Fehler ist aufgetreten, bitte melde dich beim Serverteam!");
    }

    int randomInt = ThreadLocalRandom.current().nextInt(0, possibleRewards.size());
    String rewardString = possibleRewards.get(randomInt);

    possibleRewards.remove(randomInt);
    Eastern.getRewardsFile().set(player.getUniqueId().toString(), possibleRewards);
    Eastern.getRewardsFile().save();

    Reward reward = Reward.valueOf(rewardString);
    ItemStack itemStack = new ItemStack(reward.material);
    switch (reward) {
      case WOODEN_SWORD:
      case DIAMOND_PICKAXE:
      case DIAMOND_HELMET:
      case DIAMOND_CHESTPLATE:
      case DIAMOND_LEGGINGS:
      case DIAMOND_BOOTS:
        addRandomEnchantment(itemStack);
        break;
      case CURSE_OF_BINDING:
        EnchantmentStorageMeta bindingBookMeta = (EnchantmentStorageMeta) itemStack.getItemMeta();
        bindingBookMeta.addStoredEnchant(Enchantment.BINDING_CURSE, 1, true);
        itemStack.setItemMeta(bindingBookMeta);
        break;
      case CURSE_OF_VANISHING:
        EnchantmentStorageMeta vanishingBookMeta = (EnchantmentStorageMeta) itemStack.getItemMeta();
        vanishingBookMeta.addStoredEnchant(Enchantment.VANISHING_CURSE, 1, true);
        itemStack.setItemMeta(vanishingBookMeta);
        break;
    }
    ItemMeta itemMeta = itemStack.getItemMeta();
    itemMeta.setDisplayName("§6" + reward.name);

    List<String> lore = new ArrayList<>();
    if (itemStack.getType() == Material.COOKIE) {
      lore.add("§cSchmeckt noch wie frisch gebacken!");
    } else if (itemStack.getType() == Material.COBWEB) {
      lore.add("§cBeim Frühjahrsputz gefunden, vielleicht kannst du ja was mit anfangen...");
    }
    lore.add("§aWährend des Oster-Events 2021 gefunden");

    itemMeta.setLore(lore);
    itemStack.setItemMeta(itemMeta);
    if (player.getInventory().firstEmpty() != -1) {
      player.getInventory().addItem(itemStack);
    } else {
      player.sendMessage("§cAchtung, dein Inventar ist voll!");
      player.getWorld().dropItem(player.getLocation(), itemStack);
    }
    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10f, 10f);
    return reward.name;
  }

  private static void addRandomEnchantment(ItemStack itemStack) {
    int randomInt = ThreadLocalRandom.current().nextInt(0, Enchantment.values().length);
    Enchantment randomEnchantment = Enchantment.values()[randomInt];

    boolean validEnchantmentFound = false;
    while (!validEnchantmentFound) {
      try {
        if (randomEnchantment == Enchantment.MENDING) {
          throw new IllegalArgumentException();
        }
        itemStack.addUnsafeEnchantment(randomEnchantment, 10);
        validEnchantmentFound = true;
      } catch (IllegalArgumentException exception) {
        randomInt = ThreadLocalRandom.current().nextInt(0, Enchantment.values().length);
        randomEnchantment = Enchantment.values()[randomInt];
      }
    }
  }

  public static int getEggs(Player player) {
    if (!playerEggsFound.containsKey(player.getUniqueId().toString())) {
      return 0;
    }
    return playerEggsFound.get(player.getUniqueId().toString());
  }

  public static long totalFoundEggs() {
    return playerEggsFound.values().stream().mapToLong(eggs -> eggs).sum();
  }

  public static int totalPlacement(Player player) {
    return playerEggsFound.values().stream().sorted((x, y) -> (x > y) ? -1 : ((x.equals(y)) ? 0 : 1)).collect(Collectors.toList()).indexOf(playerEggsFound.get(player.getUniqueId().toString())) + 1;
  }

}
