package net.seliba.eastern.entities;

import org.bukkit.Material;

public enum  Reward {

  DIAMOND_BLOCK("Diamantblock", Material.DIAMOND_BLOCK, 10),
  EMERALD("Emerald", Material.EMERALD, 16),
  RABBIT_SPAWN_EGG("Hasenspawnei", Material.RABBIT_SPAWN_EGG, 5),
  EGG("Ei", Material.EGG, 32),
  DIAMOND("Diamant", Material.DIAMOND, 3), // 4
  ENCHANTED_GOLDEN_APPLE("Apfel", Material.ENCHANTED_GOLDEN_APPLE, 5),
  SHULKER_SHELL("Shulkerschale", Material.SHULKER_SHELL, 2),
  NETHER_STAR("Netherstern", Material.NETHER_STAR, 1),
  DIAMOND_PICKAXE("Diamantspitzhacke", Material.DIAMOND_PICKAXE, 1),
  IRON_ORE("Eisenerz", Material.IRON_ORE, 32),
  GOLD_ORE("Golderz", Material.GOLD_ORE, 32),
  WOODEN_SWORD("Holzschwert", Material.WOODEN_SWORD, 1),
  SPAWNER("Spawner", Material.SPAWNER, 1),
  DIAMOND_HELMET("Diamanthelm", Material.DIAMOND_HELMET, 1),
  DIAMOND_CHESTPLATE("Diamantbrustplatte", Material.DIAMOND_CHESTPLATE, 1),
  DIAMOND_LEGGINGS("Diamantbeinschutz", Material.DIAMOND_LEGGINGS, 1),
  DIAMOND_BOOTS("Diamantschuhe", Material.DIAMOND_BOOTS, 1),
  BREAD("Brötchen", Material.BREAD, 16),
  CURSE_OF_VANISHING("Verzaubertes Buch", Material.ENCHANTED_BOOK, 1),
  CURSE_OF_BINDING("Verzaubertes Buch", Material.ENCHANTED_BOOK, 1),
  OAK_PLANKS("Stück Parkett", Material.OAK_PLANKS, 16),
  NAME_TAG("Namensschild", Material.NAME_TAG, 1), // 3
  HONEY_BOTTLE("Honig", Material.HONEY_BOTTLE, 5),
  NETHERITE("Netherite-Barren", Material.NETHERITE_INGOT, 5),
  COOKIE("Weihnachtskeks", Material.COOKIE, 4),
  BROWN_BANNER("Brauner Banner", Material.BROWN_BANNER, 1),
  TNT("Mini-Atombombe", Material.TNT, 1),
  CAKE("Omas Apfelkuchen", Material.CAKE, 1),
  REDSTONE("Redstone von iOser", Material.REDSTONE, 1),
  COBWEB("Spinnenweben", Material.COBWEB, 2); // 5
  // 3
  public String name;
  public Material material;
  public int amount;

  Reward(String name, Material material, int amount) {
    this.name = name;
    this.material = material;
    this.amount = amount;
  }

}
