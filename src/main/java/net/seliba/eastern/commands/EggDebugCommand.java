package net.seliba.eastern.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.seliba.eastern.entities.EggLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class EggDebugCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("test")) {
            return false;
        }

        if (args.length == 1) {
            for (Location location : EggLocation.easterEggClaimers.keySet()) {
                if (!EggLocation.easterEggClaimers.get(location).contains(Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString())) {
                    TextComponent textComponent = new TextComponent("x: " + location.getX() + " y: " + location.getY() + " z: " + location.getZ());
                    textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tp " + location.getBlockX() + " " + location.getBlockY() + " " + location.getBlockZ()));
                    sender.spigot().sendMessage(textComponent);
                }
            }
            sender.sendMessage("Eier des Spielers: " + EggLocation.playerEggsFound.get(Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString()).toString());
        } else if (args.length == 3) {
            List<String> claimers = EggLocation.easterEggClaimers.get(EggLocation.easterEggClaimers.keySet().stream().filter(location -> location.equals(new Location(Bukkit.getWorld("world"), Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2])))).findAny().get());
            sender.sendMessage(claimers.stream().map(UUID::fromString).map(Bukkit::getOfflinePlayer).map(OfflinePlayer::getName).collect(Collectors.joining(", ")));
        }
        sender.sendMessage("Insgesamte Spieler: " + EggLocation.easterEggClaimers.size() + "");
        sender.sendMessage("Insgesamte Eier: " + EggLocation.playerEggsFound.size() + "");
        return true;
    }

}
