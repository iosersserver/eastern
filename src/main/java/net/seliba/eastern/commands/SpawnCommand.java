package net.seliba.eastern.commands;

import net.seliba.eastern.Eastern;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    ((Player) sender).teleport(new Location(Bukkit.getWorld("world"), -476,171, -1392, 0, 0));
    sender.sendMessage("§aDu hast dich zum Spawn teleportiert!");
    return true;
  }

}
