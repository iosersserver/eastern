package net.seliba.eastern;

import net.seliba.eastern.commands.EggDebugCommand;
import net.seliba.eastern.commands.SpawnCommand;
import net.seliba.eastern.configuration.Config;
import net.seliba.eastern.entities.EggLocation;
import net.seliba.eastern.listener.*;
import net.seliba.eastern.scheduler.ClosestEggScheduler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Eastern extends JavaPlugin {

  private static Config dataFile = null;
  private static Config rewardsFile = null;

  @Override
  public void onEnable() {
    registerListener();
    initializeDataFile();
    EggLocation.initialize();
    ClosestEggScheduler.start();
    getCommand("eggdebug").setExecutor(new EggDebugCommand());
    getLogger().info("Erfolgreich gestartet!");
  }

  private void registerListener() {
    PluginManager pluginManager = Bukkit.getPluginManager();
    pluginManager.registerEvents(new BlockModifyListener(), this);
    pluginManager.registerEvents(new PlayerArmorstandManipulateListener(), this);
    pluginManager.registerEvents(new PlayerDamageListener(), this);
    pluginManager.registerEvents(new PlayerFoodLevelChangeListener(), this);
    pluginManager.registerEvents(new PlayerInteractEntityListener(), this);
    pluginManager.registerEvents(new PlayerInteractListener(), this);
    pluginManager.registerEvents(new PlayerJoinListener(), this);
    pluginManager.registerEvents(new PlayerPortalEnterListener(), this);
    pluginManager.registerEvents(new PlayerQuitListener(),this);
    pluginManager.registerEvents(new RedstoneListener(), this);
    pluginManager.registerEvents(new PlayerBucketEmptyListener(), this);
  }

  private void initializeDataFile() {
    dataFile = new Config("playerdata.yml", this);
    rewardsFile = new Config("rewards.yml", this);
  }

  public static Config getDataFile() {
    return dataFile;
  }

  public static Config getRewardsFile() {
    return rewardsFile;
  }

}
