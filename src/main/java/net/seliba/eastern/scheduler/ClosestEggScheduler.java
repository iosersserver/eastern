package net.seliba.eastern.scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.seliba.eastern.Eastern;
import net.seliba.eastern.entities.EggLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ClosestEggScheduler {

  public static void start() {
    Bukkit.getScheduler().runTaskLater(Eastern.getPlugin(Eastern.class), () -> {
      Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(() -> {
        Bukkit.getOnlinePlayers().stream()
            .filter(player -> EggLocation.getEggs(player) != 200)
            .forEach(player -> {
              Map<Location, Double> closestEggs = new HashMap<>();
              EggLocation.easterEggClaimers.keySet().forEach(location -> {
                if (EggLocation.easterEggClaimers.get(location).contains(player.getUniqueId().toString())) {
                  return;
                }
                closestEggs.put(location, location.distance(player.getLocation()));
              });
              double distance = closestEggs.entrySet().stream().min(Entry.comparingByValue()).get().getValue();
              int intDistance = (int) distance;
              player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§aDistanz zum nächsten Ei: " + intDistance));
            });
      }, 0, 10, TimeUnit.SECONDS);
    }, 0L);
  }

}
